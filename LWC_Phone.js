
handleInput(event) {

if (event.target.name === 'phoneNumber') {
	this.formatPhone(event.target);
	this.dataObj.phone = event.target.value;
	
}
}

formatPhone(obj) {
	var numbers = obj.value.replace(/\D/g, ""),
		char = {
			0: "(",
			3: ") ",
			6: "-"
		};
	obj.value = "";
	for (var i = 0; i < numbers.length; i++) {
		obj.value += (char[i] || "") + numbers[i];
	}
}